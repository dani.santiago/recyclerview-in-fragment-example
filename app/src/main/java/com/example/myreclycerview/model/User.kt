package com.example.myreclycerview.model

data class User(val id: Long, var name: String, var url: String)
