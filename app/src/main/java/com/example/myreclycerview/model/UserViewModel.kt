package com.example.myreclycerview.model

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class UserViewModel: ViewModel(){
    var userList = MutableLiveData<MutableList<User>>().apply {
        this.value = mutableListOf<User>(
            User(1, "Elmo", "https://pbs.twimg.com/profile_images/1092947128370884613/LN-Wm4fc_400x400.jpg"),
            User(2, "Sid", "https://www.lavanguardia.com/files/og_thumbnail/uploads/2013/01/30/5f9b1aa26a70b.jpeg"),
            User(3, "Bird bird", "https://www.wfla.com/wp-content/uploads/sites/71/2021/07/bigBirdGetty1280.jpg"),
            User(4, "Bert", "https://static.wikia.nocookie.net/muppets/images/5/51/BERT.jpg/revision/latest/scale-to-width-down/368?cb=20111215164031&path-prefix=es"),
            User(5, "Grover", "https://pbs.twimg.com/profile_images/1227761028190101504/I1zUjCpE.jpg"),
            User(6, "Ernie", "https://pbs.twimg.com/profile_images/1092454153585082368/QNM6C86g_400x400.jpg"),
            User(7, "Count von Count", "https://pbs.twimg.com/profile_images/1092451995217215490/DzhyYE8d.jpg"),
            User(8, "Oscar the Grounch", "https://www.marketingdirecto.com/wp-content/uploads/2019/11/barrio-sesamo.jpg"),
            User(9, "Kermit the Frog", "https://static.wikia.nocookie.net/disney/images/f/fb/Kermit_la_rana.png/revision/latest?cb=20160213201722&path-prefix=es"),
            User(10, "Abby Cadabby", "https://pbs.twimg.com/profile_images/1092947899770494976/jQH20EyO_400x400.jpg"),
            User(11, "Mr. Snuffleupagus", "https://static.wikia.nocookie.net/muppet/images/f/fc/Character.snuffy.jpg/revision/latest?cb=20100405201116"),
            User(12, "Julia", "https://static.wikia.nocookie.net/muppet/images/f/fd/Julia-SS.png/revision/latest?cb=20170322130351"),
            User(13, "Guy Smiley", "https://static.wikia.nocookie.net/muppet/images/9/9d/GuySmiley.png/revision/latest?cb=20140309023944"),
            User(14, "Baby Bear", "https://static.wikia.nocookie.net/muppet/images/2/2a/BearBear-blankbg.png/revision/latest?cb=20190224053704"),
            User(15, "Baby Natasha", "https://static.wikia.nocookie.net/muppet/images/1/16/Natasha.jpg/revision/latest?cb=20161119040032")
        )
    }

    var selectedUser = MutableLiveData<User>()

    fun addUser(user: User){
        userList.value!!.add(user)
        //userList.postValue(userList.value)
    }

    fun select(user: User) {
        selectedUser.postValue(user)
    }
}