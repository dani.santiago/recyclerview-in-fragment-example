package com.example.myreclycerview.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.myreclycerview.OnClickListener
import com.example.myreclycerview.R
import com.example.myreclycerview.adapter.UserAdapter
import com.example.myreclycerview.databinding.FragmentRecyclerViewBinding
import com.example.myreclycerview.model.User
import com.example.myreclycerview.model.UserViewModel

class RecyclerViewFragment : Fragment(), OnClickListener {

    private lateinit var userAdapter: UserAdapter
    private lateinit var linearLayoutManager: RecyclerView.LayoutManager
    private lateinit var binding: FragmentRecyclerViewBinding
    private val model: UserViewModel by activityViewModels()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?): View? {
        binding = FragmentRecyclerViewBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        userAdapter = UserAdapter(model.userList.value!!, this)
        linearLayoutManager = LinearLayoutManager(context)

        binding.recyclerView.apply {
            setHasFixedSize(true)
            layoutManager = linearLayoutManager
            adapter = userAdapter
        }

        model.userList.observe(viewLifecycleOwner, Observer {
            userAdapter.notifyDataSetChanged()
        })

        binding.fabAdd.setOnClickListener {
            parentFragmentManager.beginTransaction().apply {
                replace(R.id.fragmentContainerView, NewUserFragment())
                setReorderingAllowed(true)
                addToBackStack(null)
                commit()
            }
        }
    }

    override fun onClick(user: User) {
        model.select(user)
        parentFragmentManager.beginTransaction().apply {
            replace(R.id.fragmentContainerView, DetailFragment())
            setReorderingAllowed(true)
            addToBackStack(null)
            commit()
        }
    }


}