package com.example.myreclycerview.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import com.example.myreclycerview.R
import com.example.myreclycerview.databinding.FragmentNewUserBinding
import com.example.myreclycerview.model.User
import com.example.myreclycerview.model.UserViewModel

class NewUserFragment : Fragment() {

    lateinit var binding: FragmentNewUserBinding
    private val model: UserViewModel by activityViewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?): View? {
        binding = FragmentNewUserBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.buttonAdd.setOnClickListener {
            val newUser = User(model.userList.value!!.size.toLong(), binding.edittextName.text.toString(), binding.edittextImage.text.toString())
            model.addUser(newUser)
            parentFragmentManager.beginTransaction().apply {
                replace(R.id.fragmentContainerView, RecyclerViewFragment())
                commit()
            }
        }
    }
}