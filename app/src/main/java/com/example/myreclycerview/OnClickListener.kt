package com.example.myreclycerview

import com.example.myreclycerview.model.User

interface OnClickListener {
    fun onClick(user: User)
}