package com.example.myreclycerview

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.myreclycerview.databinding.ActivityMainBinding
import com.example.myreclycerview.fragments.RecyclerViewFragment

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        supportFragmentManager.beginTransaction().apply {
            replace(R.id.fragmentContainerView, RecyclerViewFragment())
            commit()
        }
    }
}